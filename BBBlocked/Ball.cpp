#include "Ball.h"

Ball::Ball() 
{
}

Ball::~Ball() 
{
}

void Ball::iniciaBall()
{
	posicoes[posYInicial] = posicoes[posY] = 538;
	posicoes[posX] = 400;
	fixaBase = true;
	bounce = false;
	collidedBlock = false;
	// carrega spritesheet da bola
	recursos.carregarSpriteSheet("ball", "./spritesheets/ball.png", 1);
	// setar spritesheet da bola
	Bola.setSpriteSheet("ball");
	// carrega audio de colisao da bola
	//recursos.carregarAudio("sound_shot", "./sons/sound_shot.wav");
	// setar audio de colisao da bola
	//somColisao.setAudio("sound_shot");
    
}

void Ball::setOnBase(bool state) 
{
	fixaBase = state;
}
bool Ball::getOnBase() 
{
	return fixaBase;
}

BounceWith Ball::desenhar(float *velocidade, int mouseX, int larguraJanela, float xBase, int *vidas)
{
	if (fixaBase)
	{
		if (mouseX >= 50 && mouseX <= larguraJanela - 50) 
		{
			posicoes[posX] = mouseX;
		}
	}
	else 
	{

		colisaoDireta = organizator.colisionBall(&bounce, &DirecaoX, &DirecaoY, posicoes[posX], posicoes[posY], xBase, &collidedBlock, &colisaoDireta);

		if (!bounce) 
		{
			if (DirecaoX == 'e') 
			{
				posicoes[posY] -= *velocidade;
				posicoes[posX] -= *velocidade;
			}
			else 
			{
				posicoes[posY] -= *velocidade;
				posicoes[posX] += *velocidade;
			}
		}
		switch (colisaoDireta) 
		{
		case Cima:
			if (DirecaoX == 'e') 
			{
				//somColisao.tocar(false);
				posicoes[posY] += *velocidade;
				posicoes[posX] -= *velocidade;
			}
			else 
			{
				//somColisao.tocar(false);
				posicoes[posY] += *velocidade;
				posicoes[posX] += *velocidade;
			}
			break;
		case Esq:
			if (DirecaoY == 's') 
			{
				//somColisao.tocar(false);
				posicoes[posY] -= *velocidade;
				posicoes[posX] += *velocidade;
			}
			else  
			{
				posicoes[posY] += *velocidade;
				posicoes[posX] += *velocidade;
			}
			break;
		case Dir:
			if (DirecaoY == 's') 
			{
				//somColisao.tocar(false);
				posicoes[posY] -= *velocidade;
				posicoes[posX] -= *velocidade;
			}
			else  
			{
				posicoes[posY] += *velocidade;
				posicoes[posX] -= *velocidade;
			}
			break;
		case colisBase:
			if (DirecaoX == 'e') 
			{
				posicoes[posY] -= *velocidade;
				posicoes[posX] -= *velocidade;
			}
			else 
			{
				posicoes[posY] -= *velocidade;
				posicoes[posX] += *velocidade;
			}
			break;
		case newBall:
			*velocidade += 1;
            *vidas -= 1;
            iniciaBall();
			break;
		}
	}
	Bola.desenhar(posicoes[posX], posicoes[posY], 0);
	//uniDesenharCirculo(posicoes[posX], posicoes[posY], 5, 10, 0, 255, 255);
	return colisaoDireta;
}

void Ball::setDirecoes(char dirX, char dirY) 
{
	DirecaoX = dirX;
	DirecaoY = dirY;
}
void Ball::setDirecaoX(char dirX) 
{
	DirecaoX = dirX;
}
void Ball::setDirecaoY(char dirY) 
{
	DirecaoY = dirY;
}

float Ball::getX() 
{
	return posicoes[posX];
}

float Ball::getY() 
{
	return posicoes[posY];
}
