#include "Level.h"

Level::Level() 
{
}

Level::~Level() 
{
}

void Level::inicializar(int level, int ancX, int ancY)
{
	iniciada = true;
	levelNumber = level;
	finalizada = true;
	ancora[ancoraX] = ancX;
	ancora[ancoraY] = ancY;
	pontos = 0;
	vidas = 3;
	//carrega spritesheet dos blocos
	recursos.carregarSpriteSheet("blue", "./spritesheets/bloco_azul.png", 1);
	recursos.carregarSpriteSheet("red", "./spritesheets/bloco_red.png", 1);
	recursos.carregarSpriteSheet("green", "./spritesheets/bloco_green.png", 1);
	recursos.carregarSpriteSheet("yellow", "./spritesheets/bloco_amarelo.png", 1);
	recursos.carregarSpriteSheet("void", "./spritesheets/void.png", 1);

	//carrega spritesheet da barra
	recursos.carregarSpriteSheet("bar", "./spritesheets/barra.png", 1);

	//seta fonte
	texto.setFonte("fonteRanking");
	texto.setCor(150, 150, 150, 255);
	lerArquivoTexto();
}

void Level::lerArquivoTexto()
{
	switch (levelNumber)
	{
	case 1:arqBlocks.open("blocks.txt");
		break;
	case 2:arqBlocks.open("blocks2.txt");
		break;
	case 3:arqBlocks.open("blocks3.txt");
		break;
	case 4:arqBlocks.open("blocks4.txt");
		break;
	}

	if (arqBlocks.is_open()) 
	{

		arqBlocks >> matrizY >> matrizX >> largura >> altura >> distBlocksX >> distBlocksY;

		matrizLevel = new Block *[matrizY];
		for (int linha = 0; linha < matrizY; linha++) 
		{
			matrizLevel[linha] = new Block[matrizX];
			for (int coluna = 0; coluna < matrizX; coluna++) 
			{
				arqBlocks >> BlockPass;
				matrizLevel[linha][coluna].inicializar(ancora[ancoraX] + coluna * (distBlocksX + largura), ancora[ancoraY] + linha * (distBlocksY + altura), std::stoi(BlockPass));
			}
		}
	}

	arqBlocks.close();
}
int Level::getPontos()
{
	return pontos;
}
int Level::getVidas()
{
    return vidas;
}

void Level::setVidas(int nvidas)
{
	vidas=nvidas;
}

void Level::desenhar(float ballX, float ballY, char *ballDirecaoX, char* ballDirecaoY, bool *collidedBlock, BounceWith *redirect, int *level)
{
	int end = 0;

	for (int linha = 0; linha < matrizY; linha++)
	{
		for (int coluna = 0; coluna < matrizX; coluna++)
		{
			if (matrizLevel[linha][coluna].getColisionPoints())
			{
				end++;
			}
			matrizLevel[linha][coluna].desenhar(ballX, ballY, ballDirecaoX, ballDirecaoY, collidedBlock, redirect);
		}
	}

	if (end == 0)
	{
		finalizar();
		levelNumber++;
		*level++;
		lerArquivoTexto();
	}
	string str = "Pontos: ";
	for (int linha = 0; linha < matrizY; linha++)
	{
		for (int coluna = 0; coluna < matrizX; coluna++)
		{
			pontos = pontos + matrizLevel[linha][coluna].desenhar(ballX, ballY, ballDirecaoX, ballDirecaoY, collidedBlock, redirect);
		}
	}
	
	/*str = str + "\nVidas: ";
	str = str + to_string(vidas);*/

	str = str + to_string(pontos);
	texto.setString(str);
    texto.desenhar(res_x / 2 - 150, res_y / 2 + -285);

    str = "Vidas: ";
    str = str + to_string(vidas);

    texto.setString(str);
    texto.desenhar(res_x / 2 + 150, res_y / 2 + -285);
}
void Level::finalizar()
{
	delete[] matrizLevel;
}