#include "libUnicornio.h"
#include "Organizador.h"

enum BlockType {
	voided, verde, amarelo, azul, vermelho,
};

class Block
{
public:
	Block();
	~Block();
	void inicializar(int PosX, int PosY, int tipo_block);
	BlockType getType() { return type; };
	int desenhar(float ballX, float ballY, char *ballDirecaoX, char *ballDirecaoY, bool *colisionBlock, BounceWith *redirect);
    int getColisionPoints();

protected:
	int MatrizX, MatrizY, ColisionPoints, rgb[3];
	BlockType type;
	Organizador organizaColision;
	Sprite Azul, Vermelho, Verde, Amarelo, Void;
	int testColision(float ballX, float ballY, char *ballDirecaoX, char *ballDirecaoY, bool *colisionBlock, BounceWith *redirect);
};