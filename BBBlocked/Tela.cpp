#include "Tela.h"

Tela::Tela() { }

Tela::~Tela() { }

void Tela::inicializar(Texto fonte)
{
	pontos.setFonte(fonte.getFonte());
	vidas.setFonte(fonte.getFonte());

	pontos.setCor(0, 0, 255);
	vidas.setCor(0, 0, 255);

	pontos.setAncora(0, 0);
	vidas.setAncora(0, 0);

	pontos.setAlinhamento(TEXTO_ALINHADO_A_ESQUERDA);
	vidas.setAlinhamento(TEXTO_ALINHADO_A_ESQUERDA);

	pontos.setEspacamentoLinhas(1.5f);
	vidas.setEspacamentoLinhas(1.5f);
	
	pontos.setString("Pontos: ");
	vidas.setString("Vidas: ");

	recursos.carregarSpriteSheet("mostruario", "spritesheets/tela_mostruarios.png", 3, 1);
	telaPonto.setSpriteSheet("mostruario");
	telaVida.setSpriteSheet("mostruario");

	telaPonto.setAncora(0, 0);
	telaVida.setAncora(0, 0);
}
void Tela::setPontos(int pont)
{
	string mudada = pontos.getString();
	ostringstream convert;
	convert << pont;
	mudada = "Pontos: " + convert.str();
	pontos.setString(mudada);
}

void Tela::setVidas(int vid)
{
	string mudada = vidas.getString();
	ostringstream convert;
	convert << vid;
	mudada = "Vidas: " + convert.str();
	vidas.setString(mudada);
}

void Tela::desenha()
{
	telaPonto.desenhar(580, 15);
	pontos.desenhar(600, 20);
	telaVida.desenhar(625, 300);
	vidas.desenhar(650, 200);
}