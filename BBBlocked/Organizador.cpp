#include "Organizador.h"

Organizador::Organizador() 
{
}

Organizador::~Organizador() 
{
}

BounceWith Organizador::colisionBall(bool *bounce, char *dirX, char *dirY, float xBall, float yBall, float xBase, bool *collidedBlock, BounceWith *colisaoDireta)
{
	bool colisBas = uniColisaoCirculoComRetangulo(xBall, yBall, 5, xBase, 550, 0.0, 100, 10, 0.5, 0.5, false);

	// Verifica colis�so contra o canto superior da tela, que deve ser uma compara�x�so de yBola menor ou igual ao dobro do raio da bola
	if (yBall <= 10) 
	{
		*bounce = true;
		bounceCima = true;
		bounceEsquerda = false;
		collidedBase = false;
		bounceDireita = false;
		*collidedBlock = false;

		*dirY = 'd';
	}
	// Verifica colis�so contra o canto esquerdo da tela, que deve ser uma compara�x�so de xBola menor ou igual ao dobro do raio da bola
	if (xBall <= 10) 
	{
		*bounce = true;
		bounceEsquerda = true;
		bounceCima = false;
		collidedBase = false;
		bounceDireita = false;
		*collidedBlock = false;

		*dirX = 'd';
	}
	// Verifica colis�so contra o canto direito da tela, que deve ser uma compara�x�so de xBola maior ou igual ao tamanho da janela menos o dobro do raio da bola
	if (xBall >= janela.getLargura() - 10) 
	{
		*bounce = true;
		bounceDireita = true;
		bounceCima = false;
		bounceEsquerda = false;
		collidedBase = false;
		*collidedBlock = false;

		*dirX = 'e';
	}
	// Verifica colis�so contra a base
	if (colisBas) 
	{
		*bounce = true;
		collidedBase = true;
		bounceCima = false;
		bounceEsquerda = false;
		bounceDireita = false;
		*collidedBlock = false;

		*dirY = 's';
	}
	// Verifica se a bola saiu da tela
	if (yBall > janela.getAltura()) 
	{
		*bounce = false;
		bounceCima = false;
		bounceEsquerda = false;
		collidedBase = false;
		bounceDireita = false;
		*collidedBlock = false;

		return newBall;
	}

	if (*collidedBlock == true) 
	{
		return *colisaoDireta;
	}

	if (bounceCima) 
	{
		return Cima;
	}
	else if (bounceDireita) 
	{
		return Dir;
	}
	else if (bounceEsquerda) 
	{
		return Esq;
	}
	else if (collidedBase) 
	{
		return colisBase;
	}
}

bool Organizador::colisionBlock(int xBall, int yBall, int xBlock, int yBlock)
{
	return uniColisaoCirculoComRetangulo(xBall, yBall, 5, xBlock, yBlock, 0.0, 30, 15, 0.5, 0.5, false);
}

BounceWith Organizador::redirectBlock(char *dirX, char *dirY, float xBall, float yBall, float xBlock, float yBlock, BounceWith *colisaoDireta)
{
	BounceWith voltar;

	if (yBall <= yBlock) 
	{
		if (*colisaoDireta != Esq && *colisaoDireta != Dir) 
		{
			bounceCima = false;
			bounceEsquerda = false;
			collidedBase = true;
			bounceDireita = false;

			*dirY = 's';
		}
	}

	if (xBall <= xBlock) 
	{
		if (*colisaoDireta != Cima && *colisaoDireta != colisBase) 
		{
			bounceEsquerda = false;
			bounceCima = false;
			collidedBase = false;
			bounceDireita = true;

			*dirX = 'd';
		}
	}

	if (xBall >= xBlock) 
	{
		if (*colisaoDireta != Cima && *colisaoDireta != colisBase) 
		{
			bounceDireita = false;
			bounceCima = false;
			bounceEsquerda = true;
			collidedBase = false;

			*dirX = 'e';
		}
	}

	if (yBall >= yBlock) 
	{
		if (*colisaoDireta != Esq && *colisaoDireta != Dir) 
		{
			bounceCima = true;
			bounceEsquerda = false;
			collidedBase = false;
			bounceDireita = false;

			*dirY = 'd';
		}
	}

	if (bounceCima == true) 
	{
		voltar = Cima;
	}
	if (bounceDireita == true) 
	{
		voltar = Dir;
	}
	if (bounceEsquerda == true) 
	{
		voltar = Esq;
	}
	if (collidedBase == true) 
	{
		voltar = colisBase;
	}
	return voltar;
}