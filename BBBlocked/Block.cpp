#include "Block.h"

Block::Block()
{
}

Block::~Block()
{
}

void Block::inicializar(int x, int y, int block)
{
	MatrizX = x;
	MatrizY = y;
	switch (block)
	{
	case 0:
		Void.setSpriteSheet("void");
		ColisionPoints = 0;
		type = voided;
		break;
	case 1:
		Verde.setSpriteSheet("green");
		type = verde;
		ColisionPoints = 1;
		break;
	case 2:
		Amarelo.setSpriteSheet("yellow");
		type = amarelo;
		ColisionPoints = 2;
		break;
	case 3:
		Azul.setSpriteSheet("blue");
		type = azul;
		ColisionPoints = 3;
		break;
	case 4:
		Vermelho.setSpriteSheet("red");
		type = vermelho;
		ColisionPoints = 4;
		break;
	}
}

int Block::desenhar(float ballX, float ballY, char *ballDirecaoX, char *ballDirecaoY, bool *colisionBlock, BounceWith *redirect)
{
	int pontos = 0;
	if (ColisionPoints > 0)
	{
		pontos=testColision(ballX, ballY, ballDirecaoX, ballDirecaoY, colisionBlock, redirect);
		if (type == azul)
			Azul.desenhar(MatrizX, MatrizY, 0);
		if (type == vermelho)
			Vermelho.desenhar(MatrizX, MatrizY, 0);
		if (type == verde)
			Verde.desenhar(MatrizX, MatrizY, 0);
		if (type == amarelo)
			Amarelo.desenhar(MatrizX, MatrizY, 0);
	}
	return pontos;
}

int Block::testColision(float ballX, float ballY, char *ballDirecaoX, char *ballDirecaoY, bool *colisionBlock, BounceWith *redirect)
{
	bool colision = organizaColision.colisionBlock(ballX, ballY, MatrizX, MatrizY);

	if (colision)
	{
		*colisionBlock = true;
		*redirect = organizaColision.redirectBlock(ballDirecaoX, ballDirecaoY, ballX, ballY, MatrizX, MatrizY, redirect);
		ColisionPoints -= 1;
		return 5;
	}
	return 0;
}

int Block::getColisionPoints() {
    return ColisionPoints;
}