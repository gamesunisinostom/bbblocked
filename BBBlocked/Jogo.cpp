#include "Jogo.h"

Jogo::Jogo()
{
}

Jogo::~Jogo()
{
}

void Jogo::inicializar()
{
	uniInicializar(700, 600, false, "BBBlocked");
	menu.inicializar();
    ball.iniciaBall();
    level.inicializar(1, 60, 60);
	
	nLevel = 1;
	status = telaInicio;
	
	//carrega audio
	recursos.carregarAudio("musica_menu", "./sons/The Mass Effect Soundtrack- The Normandy.wav");
	
	//carrega imagens
	recursos.carregarSpriteSheet("telaAjuda", "./spritesheets/tela_ajuda.png", 1);
	recursos.carregarSpriteSheet("telaCreditos", "./spritesheets/tela_creditos.png", 1);
	recursos.carregarSpriteSheet("fundoJogo", "./spritesheets/galaxia_fundo.png", 1);
	
	//seta audio
	mus_menu.setAudio("musica_menu");
	pontos = 0;
	vidas = 4;
	firstStart = true;
	
	//seta barra
	Bar.setSpriteSheet("bar");
	
	//seta imagens
	telaAjuda.setSpriteSheet("telaAjuda");
	telaCreditos.setSpriteSheet("telaCreditos");
	fundoJogo.setSpriteSheet("fundoJogo");
	
	//seta fonte
	texto.setFonte("fonteRanking");
	texto.setCor(150, 150, 150, 255);

}

void Jogo::finalizar()
{
	level.finalizar();
	menu.finalizar();
	uniFinalizar();
}

void Jogo::executar()
{
	xBase = 400;
	velocidade = 1.5;

	while (!teclado.soltou[TECLA_ESC] && !aplicacao.sair && status != outGame)
	{
		uniIniciarFrame();

		switch (status)
		{
		case telaInicio:
			menu.desenhar();
			mus_menu.tocar(true);
			menu.atualiza(status);
			break;
		case telaRanking:
			leArquivoRanking();
			if (teclado.pressionou[TECLA_ENTER] || teclado.pressionou[TECLA_ENTER2])
			{
				status = telaInicio;
			}
			break;
		case telaHelp:
			telaAjuda.desenhar(janela.getLarguraTela() / 2, janela.getAlturaTela() / 2);
			if (teclado.pressionou[TECLA_ENTER] || teclado.pressionou[TECLA_ENTER2])
			{
				status = telaInicio;
			}
			break;
		case telaCredits:
			telaCreditos.desenhar(janela.getLarguraTela() / 2, janela.getAlturaTela() / 2);
			if (teclado.pressionou[TECLA_ENTER] || teclado.pressionou[TECLA_ENTER2])
			{
				status = telaInicio;
			}
			break;
		case startGame:
			mus_menu.parar();
			fundoJogo.desenhar(janela.getLarguraTela() / 2, janela.getAlturaTela() / 2);
			gameStart();
			if (teclado.pressionou[TECLA_ENTER] || teclado.pressionou[TECLA_ENTER2])
			{
				mus_
				status = telaInicio;
			}
			break;
		}
		uniTerminarFrame();
	}
}

void Jogo::gameStart()
{
	mouse.esconderCursor();
	mx = mouse.x;

	if (teclado.soltou[TECLA_R])
	{
		ball.iniciaBall();
		level.finalizar();
		level.inicializar(1, 60, 60);
	}

	if (mouse.pressionou[0] && ball.getOnBase())
	{
		ball.setDirecoes('e', 's');
		ball.setOnBase(false);
	}
	else if (mouse.pressionou[2] && ball.getOnBase())
	{
		ball.setDirecoes('d', 's');
		ball.setOnBase(false);
	}

	if (mx >= 50 && mx <= janela.getLargura() - 50)
	{
		xBase = mx;
	}
    int vidas = level.getVidas();
	level.desenhar(ball.getX(), ball.getY(), &ball.DirecaoX, &ball.DirecaoY, &ball.collidedBlock, &ball.colisaoDireta, &nLevel);
    an = ball.desenhar(&velocidade, mx, janela.getLargura(), xBase, &vidas);
    level.setVidas(vidas);

	if (vidas == -1)
	{
		status = telaRanking;
		mouse.mostrarCursor();
		level.setVidas(vidas);
		pontos = level.getPontos();
		mandaProRanking();
	}
	Bar.desenhar(xBase, 553, 0);
}

void Jogo::mandaProRanking()
{
	int *ranking, numRaking = 0, aux;
	ofstream ArquivoSaida;
	ifstream ArquivoEntrada;
	bool inserido = false;
	ArquivoEntrada.open("ranking.txt");
	ArquivoEntrada >> numRaking;
	ranking = new int[numRaking];
	for (int i = 0; i < numRaking; i++)
		ArquivoEntrada >> ranking[i];
	ArquivoEntrada.close();
	ArquivoSaida.open("ranking.txt");
	if (numRaking < 10)
		ArquivoSaida << numRaking + 1;
	else
		ArquivoSaida << 10;
	for (int i = 0; i < numRaking; i++)
	{
		if (ranking[i] > pontos)
			ArquivoSaida << " " << ranking[i];
		else if (ranking[i] < pontos)
		{
			if (!inserido)
				ArquivoSaida << " " << pontos << " " << ranking[i];
			else
				ArquivoSaida << " " << ranking[i];
			inserido = true;
		}
		else
		{
			if (!inserido)
				ArquivoSaida << " " << pontos << " " << ranking[i];
			else
				ArquivoSaida << " " << ranking[i];
			inserido = true;
		}
	}
	if (!inserido)
		ArquivoSaida << " " << pontos;
	ArquivoSaida.close();
	delete[] ranking;
}
void Jogo::leArquivoRanking()
{
	//DAQUI 
	int *ranking, numRaking = 0, aux;
	ifstream ArquivoEntrada;
	bool inserido = false;
	ArquivoEntrada.open("ranking.txt");
	ArquivoEntrada >> numRaking;
	ranking = new int[numRaking];
	for (int i = 0; i < numRaking; i++)
		ArquivoEntrada >> ranking[i];
	ArquivoEntrada.close();
	str = "TOP 10\n";
	for (int i = 0; i < numRaking; i++)
		str = str + to_string(ranking[i]) + "\n";
	texto.setString(str);
	delete[] ranking;
	texto.desenhar(100, res_y / 2);
	//ATEH AQUI
}