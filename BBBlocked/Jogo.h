#pragma once
#include "libUnicornio.h"
#include "Ball.h"
#include "Base.h";
#include "Menu.h";
#include "Tela.h"
#include "Level.h";
#include <fstream>
#include <iostream>

class Jogo
{
public:
	Jogo();
	~Jogo();

	void inicializar();
	void finalizar();
	void telaInicial();
	void gameStart();
	void addPontos();
	int getPontos();
	void executar();
	void mandaProRanking();
	void leArquivoRanking();

protected:
	Menu menu;
	Status status;
	ifstream arqBlocks;
	ofstream arqteste;
	Level level;
	Base base;
	Tela telaIni;
	Som mus_menu;
	Tela tela;
	Ball ball;
	Sprite Bar, telaAjuda, telaCreditos, fundoJogo;
	Texto texto;
	float velocidade;
	int mx, xBase, pontos, vidas, nLevel;
	BounceWith an;
	bool firstStart;
	string str;
};
