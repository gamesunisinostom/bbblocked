#include "libUnicornio.h"

enum Status{
	telaInicio, startGame, gameStart, endGame, outGame, telaHelp, pauseGame, telaRanking, telaCredits,
};

class Menu
{
public:
	Menu();
	~Menu();

	void inicializar();
	void finalizar();

	void atualiza(Status &status);
	void desenhar();

private:
	BotaoSprite jogar;
	BotaoSprite help;
	BotaoSprite ranking;
	BotaoSprite credits;
	BotaoSprite sair;
	Sprite telaInicial, telaAjuda;
	Som  mus_menu, mus_game;
	Texto texto;

};
