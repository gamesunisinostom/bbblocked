#include "Ranking.h"


Ranking::Ranking()
{
	statusRanking = telaRanking;
}

Ranking::~Ranking()
{
}


void Ranking::inicializar()
{
	//	carrega os spritesheets para os botoes
	recursos.carregarSpriteSheet("button_voltar", "dados/spritesheets/button_voltar.png", 3, 1);

	//	setar spritesheet nos botoes
	buttonVoltar.setSpriteSheet("button_voltar");

	//	posiciona os botoes
	buttonVoltar.setPos(res_x - res_x / 6, res_y - res_y / 6);
	/*
	recursos.carregarFonte("fonteRanking", "./fontes/FantasqueSansMono-Regular.ttf", 48);
	texto.setFonte("fonteRanking");
	texto.setCor(0, 0, 255, 255);
	*/
}

void Ranking::atualizar()
{
	statusRanking = telaRanking;
	if (statusRanking == telaRanking){
		//	atualiza todos os botoes
		buttonVoltar.atualizar();

		//	processa os cliques nos botoes
		if (buttonVoltar.estaClicado())
		{
			statusRanking = 'M';
		}
	}
}

void Ranking::desenhar()
{
	//	desenha os botoes
	buttonVoltar.desenhar();
	
	escreverRanking();
}

void Ranking::escreverRanking()
{
	int *ranking, numRanking, aux;
	ifstream ArquivoEntrada;
	string rankingEscrito;

	ArquivoEntrada.open("ranking.txt");

	ArquivoEntrada >> numRanking;
	ranking = new int[numRanking];

	for (int i = 0; i < numRanking; i++)
		ArquivoEntrada >> ranking[i];

	ArquivoEntrada.close();

	rankingEscrito = "";
	for (int i = 0; i < numRanking; i++)
	{
		if (ranking[i] < 10)
			rankingEscrito = rankingEscrito + "00000" + to_string(ranking[i]) + "\n";
		else if (ranking[i] < 100)
			rankingEscrito = rankingEscrito + "0000" + to_string(ranking[i]) + "\n";
		else if (ranking[i] < 1000)
			rankingEscrito = rankingEscrito + "000" + to_string(ranking[i]) + "\n";
		else if (ranking[i] < 10000)
			rankingEscrito = rankingEscrito + "00" + to_string(ranking[i]) + "\n";
		else if (ranking[i] < 100000)
			rankingEscrito = rankingEscrito + "0" + to_string(ranking[i]) + "\n";
		else if (ranking[i] < 1000000)
			rankingEscrito = rankingEscrito + to_string(ranking[i]);
	}

	texto.setString(rankingEscrito);

	texto.desenhar(res_x / 2, res_y / 2);
	delete[] ranking;
}

char Ranking::getStatusRanking()
{
	return statusRanking;
}