#pragma once
#include "libUnicornio.h"
#include <iomanip>
#include <locale>
#include <sstream>

class Tela
{
public:
	Tela();
	~Tela();
	void desenha();
	void inicializar(Texto fonte);
	void setPontos(int pontos);
	void setVidas(int vidas);
	void perdeVida();
	void ganhaVida();


protected:
	Texto pontos, vidas;
	Sprite telaPonto, telaVida;
};

