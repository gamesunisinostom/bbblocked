#include "Menu.h"

Menu::Menu()
{
}

Menu::~Menu()
{
}

void Menu::inicializar()
{
	//	carrega os spritesheets para os botoes
	recursos.carregarSpriteSheet("botao_start", "./spritesheets/button_iniciar.png", 3, 1);
	recursos.carregarSpriteSheet("botao_help", "./spritesheets/button_help.png", 3, 1);
	recursos.carregarSpriteSheet("botao_credits", "./spritesheets/button_credits.png", 3, 1);
	recursos.carregarSpriteSheet("botao_exit", "./spritesheets/button_sair.png", 3, 1);
	recursos.carregarSpriteSheet("botao_ranking", "./spritesheets/button_rank.png", 3, 1);
	
	//carrega imagens
	recursos.carregarSpriteSheet("telaInicial", "./spritesheets/tela_inicial.png", 1);
	

	//carrega e seta fonte
	recursos.carregarFonte("fonteRanking", "./fontes/FantasqueSansMono-Regular.ttf", 48);
	texto.setFonte("fonteRanking");
	texto.setCor(0, 0, 255, 255);
	
	//carrega musicas
	recursos.carregarAudio("musica_game", "./sons/Final Fantasy VIII Soundtrack - Battle Theme [Don't Be Afraid].wav");

	//	setar spritesheet nos botoes
	jogar.setSpriteSheet("botao_start");
	credits.setSpriteSheet("botao_credits");
	ranking.setSpriteSheet("botao_ranking");
	help.setSpriteSheet("botao_help");
	sair.setSpriteSheet("botao_exit");

	//	posiciona os botoes
	jogar.setPos(janela.getLarguraTela() / 2, janela.getAlturaTela() / 2 -45);
	ranking.setPos(janela.getLarguraTela()/ 2 + 6, janela.getAlturaTela() / 2 + 175);
	help.setPos(janela.getLarguraTela() / 2 + 2, janela.getAlturaTela() / 2 + 23);
	credits.setPos(janela.getLarguraTela() / 2, janela.getAlturaTela() / 2 + 100);
	sair.setPos(janela.getLarguraTela() / 2, janela.getAlturaTela() / 2 + 240);

	//setar imagem da tela inicial
	telaInicial.setSpriteSheet("telaInicial");
	

	//setar audio
	mus_game.setAudio("musica_game");
	

}

void Menu::finalizar()
{
	//	descarregar spritesheets
	recursos.descarregarSpriteSheet("botao_start");
	recursos.descarregarSpriteSheet("botao_ranking");
	recursos.descarregarSpriteSheet("botao_credits");
	recursos.descarregarSpriteSheet("botao_help");
	recursos.descarregarSpriteSheet("botao_exit");
	recursos.descarregarSpriteSheet("telaInicial");
	recursos.descarregarSpriteSheet("telaAjuda");
}

void Menu::atualiza(Status &status)
{
	mus_menu.tocar(true);

	//	atualiza todos os botoes
	jogar.atualizar();
	credits.atualizar();
	ranking.atualizar();
	help.atualizar();
	sair.atualizar();

	if (jogar.estaClicado())
	{
		status = startGame;
		mus_game.tocar(true);
	}
	if (help.estaClicado()) 
	{
		status = telaHelp;
	}
	if (ranking.estaClicado()) 
	{
		status = telaRanking;
	}
	if (credits.estaClicado()) 
	{
		status = telaCredits;
	}
	if (sair.estaClicado()) 
	{
		status = outGame;
	}
}

void Menu::desenhar()
{
   
	//	desenha os botoes
	telaInicial.desenhar(janela.getLarguraTela() / 2, janela.getAlturaTela() / 2);
	jogar.desenhar();
	credits.desenhar();
	ranking.desenhar();
	help.desenhar();
	sair.desenhar();
	
}
