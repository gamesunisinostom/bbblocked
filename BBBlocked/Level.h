#include "Block.h"
#include "Organizador.h"
#include <iostream>
#include <fstream>
#include <string>

enum AncoraFase {
	ancoraX, ancoraY
};

class Level
{
public:
	Level();
	~Level();
	void inicializar(int numberLevel, int ancX, int ancY);
	void desenhar(float ballX, float ballY, char *ballDirecaoX, char *ballDirecaoY, bool *collidedBlock, BounceWith *redirect, int *level);
	void finalizar();
	int getPontos();
    int getVidas();
	void setVidas(int nvidas);

    int pontos, vidas;

protected:
	
	Block** matrizLevel;
	ifstream arqBlocks;
	ofstream arqAleatorio;
	string BlockPass;
	int levelNumber, matrizY, distBlocksX, largura, altura, distBlocksY, matrizX, ancora[2];
	Texto texto;
	bool iniciada, finalizada;
	void lerArquivoTexto();
};