#pragma once
#include <ctime>
#include <math.h>
#include "libUnicornio.h"

enum Directions {
	CimaDireita, BaixoDireita, CimaEsquerda, BaixoEsquerda
};

enum BounceWith {
	colisBase, newBall, Cima, Esq, Dir, nenhum
};

class Organizador
{
public:
	Organizador();
	~Organizador();
	BounceWith colisionBall(bool *bounce, char *dirX, char *dirY, float xBall, float yBall, float xBase, bool *collidedBlock, BounceWith *colisaoDireta);
	bool colisionBlock(int xBall, int yBall, int xBlock, int yBlock);
	BounceWith redirectBlock(char *dirX, char *dirY, float xBall, float yBall, float xBlock, float yBlock, BounceWith *colisaoDireta);

protected:
	int gambi = 0;
	bool click = false, bounceCima = false, bounceEsquerda = false, bounceDireita = false, collidedBase = false;
	BounceWith colisaoDireta;
};
