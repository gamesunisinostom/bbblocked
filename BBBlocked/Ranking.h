#pragma once
#include "libUnicornio.h"
#include "Menu.h"
#include <fstream>

class Ranking
{
public:
	Ranking();
	~Ranking();

	void inicializar();
	void atualizar();
	void desenhar();
	void escreverRanking();
	char getStatusRanking();
	
private:
	BotaoSprite buttonVoltar;
	char statusRanking;
	Texto texto;
};