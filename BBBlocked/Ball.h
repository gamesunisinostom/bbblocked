#include "Organizador.h"
#pragma once

enum Posicao {
	posX, posY, posYInicial
};

class Ball
{
public:
	Ball();
	~Ball();
    void iniciaBall();
	void setOnBase(bool state);
	void setDirecoes(char dirX, char dirY);
	void setDirecaoX(char dirX);
	void setDirecaoY(char dirY);
	BounceWith desenhar(float *velocidade, int mouseX, int larguraJanela, float xBase, int * vidas);
	float getX();
	float getY();
	bool getOnBase();

	char DirecaoX, DirecaoY;
	bool collidedBlock;
	BounceWith colisaoDireta;

protected:
	float posicoes[3];
	bool fixaBase, bounce;
	Organizador organizator;
	Sprite Bola;
	Som somColisao;
};
